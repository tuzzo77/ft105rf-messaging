package com.econorma.push.android;

import java.util.ArrayList;
import java.util.List;

/*
{ 
	  "data": {
	    "title": "puta",
	    "message": "Test Message"
	  },
	  "registration_ids": ["APA91bF0q8ZCCdVFIa8zwoCCDkM708ShCSXUxdVOlK1JbQ17AKDm8q3IHmtaIBGNp3h_sdEbYSVdAo1QLdt4mxh8vCkaA453qbirPK9U9APPsyv4j_wAWQm01FfiQLfvjn83ODH62qVgHlOtLXU1B_n-NaXE0SRJMh1zH_Q5l4jA33Wq-hYzuoE"] 
	}
*/
public class FcmMessage<T extends FcmMessage.PayLoad> {

	private T data;
	private T notification;	
	
	
	public T getData() {
		return data;
	}


	public void setData(T data) {
		this.data = data;
	}
	

	public T getNotification() {
		return notification;
	}


	public void setNotification(T notification) {
		this.notification = notification;
	}




	private List<String> registration_ids = new ArrayList<String>();
	
	protected void addTargets(List<String> targets){
		registration_ids.addAll(targets);
	}
 
	
	
	public static abstract class PayLoad {
		
		private String collapse_key;

		public PayLoad(String collapse_key){
			setType(collapse_key);
		}
		public String getType() {
			return collapse_key;
		}

		public void setType(String collapse_key) {
			this.collapse_key = collapse_key;
		}	
	}

	
}
