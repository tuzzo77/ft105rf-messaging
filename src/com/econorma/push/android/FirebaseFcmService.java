package com.econorma.push.android;

import java.io.IOException;
import java.util.List;

import org.eclipse.jetty.client.ContentExchange;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.io.ByteArrayBuffer;

import com.econorma.push.android.FcmMessage.PayLoad;
import com.econorma.util.Logger;
import com.google.gson.Gson;

public class FirebaseFcmService {
	
	private static final Logger logger = Logger.getLogger(FirebaseFcmService.class);
	private static final String TAG = "FirebaseFcmService";
	
	private static String API_URL = "https://fcm.googleapis.com/fcm/send";
	private static String HEADER_KEY_AUTHORIZATION ="Authorization";
	private static String HEADER_VALUE_AUTHORIZATION ="key=AAAAe2esvIY:APA91bFvoSvMeckwjftfacn6LTrltTDOVU_XLTvzMT_tAdLVeXYksobyzaRvZvjQieKzZ6YYNwuQoVDws2oFxyVZBFA4wbU2Q5mCljgkkVfWgd_bOLc3F1_rzxWpTT8JR4zxwH9EDtdE";

	private final HttpClient client;

	public FirebaseFcmService() {
		client = new HttpClient();
		client.setTimeout(30000); // 30 seconds timeout; if no server reply, the request expires
		try {
			client.start();
		} catch (Exception e) {
			logger.error("WTF while creating GoogleGdmService", e);
		}
	}

	public <T extends PayLoad> FcmMessage<T> createMessage(Class<T> clz){
		FcmMessage<T> message = new FcmMessage<T>();
		return message;
	}
	
	public void send(AlarmFcmNotification alarmNotification, List<String> registrations_ids){
		
		for (String token: registrations_ids){
			alarmNotification.setTo(token);
			_send(alarmNotification);
		}
	
	}
	
	public <T extends PayLoad> void _send(AlarmFcmNotification alarmNotification){
		
//		message.addTargets(registrations_ids);
		
		ContentExchange exchange = new ContentExchange();
		exchange.setMethod("POST");
		exchange.setURL(API_URL);
		
		String json = new Gson().toJson(alarmNotification);
		
		//http://download.eclipse.org/jetty/stable-8/apidocs/
		
		exchange.addRequestHeader(HEADER_KEY_AUTHORIZATION, HEADER_VALUE_AUTHORIZATION);
		exchange.setRequestContentType("application/json");
		ByteArrayBuffer buffer = new ByteArrayBuffer(json.getBytes());
		exchange.setRequestContent(buffer);
		try {
			client.send(exchange);
			// Waits until the exchange is terminated
			
			int exchangeState = exchange.waitForDone();
			
			
			String responseContent = exchange.getResponseContent();
			int status = exchange.getStatus();
			logger.info(TAG, "Response: " + responseContent +  " status: " + status);
			switch (status) {
			case 200:
				//ok
				break;

			default:
				break;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	 
	
}
