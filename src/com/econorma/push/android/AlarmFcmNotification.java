package com.econorma.push.android;

public class AlarmFcmNotification {
	
	private String to;
	private String collapse_key;
	private Notification notification;
	private Data data; 
	
	public String getTo() {
		return to;
	}


	public void setTo(String to) {
		this.to = to;
	}


	public String getCollapse_key() {
		return collapse_key;
	}


	public void setCollapse_key(String collapse_key) {
		this.collapse_key = collapse_key;
	}


	public Notification getNotification() {
		return notification;
	}


	public void setNotification(String body, String title) {
		Notification notification = new Notification();
		notification.setBody(body);
		notification.setTitle(title);
		this.notification = notification;
	}


	public Data getData() {
		return data;
	}


	public void setData(String body, String title, String key1, String key2) {
		Data data = new Data();
		data.setBody(body);
		data.setTitle(title);
		data.setKey_1(key1);
		data.setKey_2(key2);
		this.data = data;
	}


	public class Notification {
		
		private String body;
		private String title;
		
		public String getBody() {
			return body;
		}
		public void setBody(String body) {
			this.body = body;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		
	}

	
	public class Data {
		
		private String body;
		private String title;
		private String key_1;
		private String key_2;
		public String getBody() {
			return body;
		}
		public void setBody(String body) {
			this.body = body;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getKey_1() {
			return key_1;
		}
		public void setKey_1(String key_1) {
			this.key_1 = key_1;
		}
		public String getKey_2() {
			return key_2;
		}
		public void setKey_2(String key_2) {
			this.key_2 = key_2;
		}
		
	}
}
