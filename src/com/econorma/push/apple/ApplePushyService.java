package com.econorma.push.apple;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.eatthepath.pushy.apns.ApnsClient;
import com.eatthepath.pushy.apns.ApnsClientBuilder;
import com.eatthepath.pushy.apns.PushNotificationResponse;
import com.eatthepath.pushy.apns.util.ApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPushNotification;
import com.eatthepath.pushy.apns.util.TokenUtil;
import com.eatthepath.pushy.apns.util.concurrent.PushNotificationFuture;
import com.econorma.util.Logger;

public class ApplePushyService {

	private static final Logger logger  = Logger.getLogger(ApplePushyService.class);
	private static final String TAG = "ApplePushyService";
	
	private static String CERT_DIR = "./certificates/";
	private static String CERT = "Last_Certificati.p12";
	private static String PASSWORD ="patitaga";
	private static String BUNDLE = "com.econorma.ECONORMA";
	private ApnsClient apnsClient;

	public ApplePushyService(){
		
		try {
			 apnsClient = new ApnsClientBuilder()
					.setApnsServer(ApnsClientBuilder.PRODUCTION_APNS_HOST)
					.setClientCredentials(new File(CERT_DIR+CERT), PASSWORD)
					.build();  	
		} catch (Exception e) {
			logger.error(TAG, "Failed to instantiate push notification");
		}
		
	}

	public void send(String message, List<String> registrations_ids){

		for (String token: registrations_ids){
			String messagge = "Allarme Sonda" + "\n" + message.replaceAll("\\n", " ");
			_send(token, messagge);

		}

	}
 
	public void _send(String deviceId, String message) {
		
		try {
			
			final SimpleApnsPushNotification pushNotification;  

			final ApnsPayloadBuilder payloadBuilder = new SimpleApnsPayloadBuilder();

			payloadBuilder.setAlertTitle("Errore Server");
			payloadBuilder.setAlertBody(message);  

			final String payload = payloadBuilder.build();  

			final String token = TokenUtil.sanitizeTokenString(deviceId);  

			pushNotification = new SimpleApnsPushNotification(token, BUNDLE, payload);  

			try {
				final PushNotificationFuture<SimpleApnsPushNotification, PushNotificationResponse<SimpleApnsPushNotification>> sendNotificationFuture = apnsClient.sendNotification(pushNotification);  

				final PushNotificationResponse<SimpleApnsPushNotification> pushNotificationResponse = sendNotificationFuture.get();
				if (pushNotificationResponse.isAccepted()) {
					logger.info(TAG, "Push notification accepted by APNs gateway"); 
				} else {
					logger.error(TAG, "Notification rejected by the APNs gateway:" + pushNotificationResponse.getRejectionReason());
			
					if (pushNotificationResponse.getTokenInvalidationTimestamp() != null) {
						logger.error(TAG, "\\t…and the token is invalid as of " + pushNotificationResponse.getTokenInvalidationTimestamp());
					}
				}
			} catch (final ExecutionException e) {
				logger.error(TAG, "Failed to send push notification");
			}


		} catch (Exception e) {
			logger.error(TAG, e);
		}

	}


}
