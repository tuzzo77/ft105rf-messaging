package com.econorma.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

public class CreateCertificates {
	
	private static final String TAG = CreateCertificates.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(CreateCertificates.class);
	private static final String CERT="Last_Certificati.p12";
	
	public CreateCertificates() {
		
	}
	
	public void createCertFolder() 	{
		  
		 writeFile(CERT);
	}

private void writeFile(String cert){
	
	if (new File("certificates").mkdir()) {
		logger.info(TAG, "Creata cartella certificati");
	}
	
	File f = new File("certificates/" +cert);
	boolean load=false;
	
	try {
		if (f.exists()){
			load=true;
		}
		
	} catch (Exception e) {
		}finally{
	}
	
 	
	if (!load){
	
	InputStream is=null;
	OutputStream out =null;
	
	try {
	 	
		is = getCertStream(cert);
		
		 
		    int readBytes;
		    byte[] buffer = new byte[4096];
		    try {
		    	out = new FileOutputStream(f);
		        while ((readBytes = is.read(buffer)) > 0) {
		            out.write(buffer, 0, readBytes);
		        }
		    } catch (IOException e) {
		    	logger.error(TAG, "Errore in creazione maps : "+  e.toString());
		    }
  
		
	} catch (Exception e) {

	} finally{
		IOUtils.closeQuietly(is);
		IOUtils.closeQuietly(out);
	}
  }
	
 
}


private InputStream getCertStream(String cert){
	InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/econorma/resources/" + cert);
	logger.debug(TAG, "Resource stream : "+ (in!=null));
	return in;
}

}
