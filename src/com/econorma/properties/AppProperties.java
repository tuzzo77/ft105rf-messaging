package com.econorma.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AppProperties {
	
	private static AppProperties instance = null;
	
	 public static AppProperties getInstance() {
	      if(instance == null) {
	         instance = new AppProperties();
	      }
	      return instance;
	   }

	public void create(){
	 
		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream("application.properties");

			prop.setProperty("ip", "188.14.207.149");
			prop.setProperty("port", "6161");
			prop.setProperty("minutes_delay", "1");
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public Map<String, String> load(){
		
		Map<String, String> map = new HashMap<String, String>();
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("application.properties");

			prop.load(input);
			
			for (final String name: prop.stringPropertyNames()) {
				map.put(name, prop.getProperty(name));
			}
			 

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return map;
	}
	
 
	
}
