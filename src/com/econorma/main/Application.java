package com.econorma.main;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jdesktop.application.SingleFrameApplication;

import com.econorma.logic.AutoSenderManager;
import com.econorma.logic.ReadDevices;
import com.econorma.properties.AppProperties;

 

public class Application extends SingleFrameApplication  {

	private static final String TAG = Application.class.getSimpleName();
	private int state;
 	private ExecutorService executorService = Executors
			.newSingleThreadExecutor();
 	private ReadDevices readDevices;
 	private String ipAddress;
 	private String port;
 	private int minutesDelay;
 	private AutoSenderManager autoSenderManager;

	public void init() {
		Map<String, String> map = AppProperties.getInstance().load();
		ipAddress  = map.get("ip");
		port = map.get("port");
		minutesDelay = Integer.parseInt(map.get("minutes_delay"));
	}
 

	@Override
	protected void startup() {
		init();
		autoSenderManager = new AutoSenderManager(minutesDelay);
		autoSenderManager.run();
		
//		exit();
 	}
	
	@Override
	protected void shutdown() {
		super.shutdown();
	}

	 
	public static synchronized Application getInstance() {
		return (Application) org.jdesktop.application.Application.getInstance();
	}
	
	 

	public ExecutorService getExecutorservice(){
		return executorService;
	}


	public ReadDevices getReadDevices() {
		return readDevices;
	}


	public void setReadDevices(ReadDevices readDevices) {
		this.readDevices = readDevices;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
	
}
