package com.econorma.main;

import java.io.File;

import javax.swing.JOptionPane;

import com.econorma.properties.AppProperties;
import com.econorma.util.CreateCertificates;
import com.econorma.util.JustOneLock;
import com.econorma.util.Logger;


public class Main {

	private static final String TAG = Main.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) {

		JustOneLock ua = new JustOneLock(Main.class.getName());

		Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler());
		
		try {
			File f = new File("application.properties");
			if(!f.exists()) { 
				AppProperties.getInstance().create();
			}
		}catch (Exception e) {
			logger.error(TAG, "Errore in creazione file properties");
		}

		try {
			CreateCertificates certificates = new CreateCertificates();
			certificates.createCertFolder();

		}catch (Exception e) {
			logger.error(TAG, "Errore in creazione certificato");
		}

		if (ua.isAppActive()) {
			logger.info(TAG, "Another istance is running...");
			JOptionPane.showMessageDialog(null, "Programma gi� in esecuzione",
					"Econorma", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} else {

			logger.info(TAG, "No other instance running");
			Application.launch(Application.class, args);

		}

	}


}
