package com.econorma.main;

import java.lang.Thread.UncaughtExceptionHandler;

import com.econorma.util.Logger;

public class DefaultExceptionHandler implements UncaughtExceptionHandler{

	private static final String TAG = "UnchaughExceptionHandler";
	private static final Logger logger = Logger.getLogger(DefaultExceptionHandler.class);
	
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		logger.error(TAG, "E' stata registrata un'eccezione non gestita nel thread: "+thread.getName(), ex);
		
	}

}
