package com.econorma.logic;

import java.io.FileReader;
import java.io.Reader;

import com.econorma.util.Logger;
import com.google.gson.Gson;

public class ReadMails {
	
	private static final Logger logger = Logger.getLogger(ReadMails.class);
	private static final String TAG = "ReadMails";
	private static final String FILE_NAME = "mails.json";
	
	public ReadMails() {
		
	}
	
	public String[] getMails() {
		
		try {
			Reader reader = new FileReader(FILE_NAME);
			 Gson gson = new Gson();
	            String[] emails = gson.fromJson(reader, String[].class);
	            return emails;
					
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		
		return null;
		 
	}

}
