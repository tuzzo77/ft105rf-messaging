package com.econorma.logic;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.econorma.util.Logger;

public class AppleCurlMessage {
	
	private static final String TAG = AppleCurlMessage.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(AppleCurlMessage.class);
	
	private static final String BUNDLE_ID = "com.econorma.ECONORMA";
	private static final String CERT = "Last_Certificati.p12";
	private static final String PASSWORD = "patitaga";
	private static final String API_URL = "https://api.push.apple.com/3/device/";
	private static final String TYPE = "--http2";
	
	private String message;
	private String token;
	
//	curl -v \
//	-d '{"aps":{"alert":"<message>","badge":42}}' \
//	-H "apns-topic: com.econorma.ECONORMA" \
//	-H "apns-priority: 10" \
//	--http2 \
//	--cert-type P12 --cert Last_prod_sandbox.p12:patitaga \
//	https://api.push.apple.com/3/device/3cd30372e136ee9343c1a303dd5489a2116ef665f60ef1e01c353a2095d23ebc
	
	public AppleCurlMessage(String messagge, String token) {
		this.message=messagge;
		this.token=token;
	}
	
	public void send() {
		
		try {
		String url_command = "curl -v" + 
							" -d " +  "'{\"aps\":{\"alert\":\"<message>\",\"badge\":42}}'" +
							" -H " + "apns-topic: " + BUNDLE_ID  +
							" -H apns-priority: 10" + 
							" " + TYPE+ 
							" --cert-type P12 --cert " + CERT +  ":" + PASSWORD +
							" " + API_URL + token;
		
		
 			Process process = Runtime.getRuntime().exec(url_command);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

				BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
				
							String s = null;
				while ((s = stdInput.readLine()) != null) {
					logger.info(TAG, "Command output:" + s);
				}

				System.out.println("Here is the standard error of the command (if any):\n");
				while ((s = stdError.readLine()) != null) {
					logger.info(TAG, "Error command:" + s);
				}
			
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

}
