package com.econorma.logic;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

import com.econorma.main.Application;
import com.econorma.util.Logger;

public class PingServer {
	
	private static final Logger logger  = Logger.getLogger(PingServer.class);
	private static final String TAG = "PingServer";
	
	private String port;
	private String ipAddress;
	private String url;
	
	public PingServer() {
		port = Application.getInstance().getPort();
		ipAddress = Application.getInstance().getIpAddress();
		url = "http://" + ipAddress + ":" + port;
 	}
	
	public boolean isReachable()  {
		
		boolean reply = false;
		final int MAX_RETRY = 3;
		int retry_count = 0;
	
		while(retry_count<MAX_RETRY){
			if(connect(url)){
				logger.info(TAG, "Server raggiungibile: " + url);
				reply = true;
				break;
			}else{
				logger.error(TAG, "Server non raggiungibile: "+ url);
				reply = false;
			}
			retry_count++;
		}
		
		return reply;

	}
	
	
	
	public boolean connect(String targetUrl) {
		try {
			HttpURLConnection httpUrlConnection = (HttpURLConnection) new URL(targetUrl).openConnection();
			httpUrlConnection.setRequestMethod("HEAD");
			httpUrlConnection.setConnectTimeout(5000);
			httpUrlConnection.setReadTimeout(10000);

			try  {
				int responseCode = httpUrlConnection.getResponseCode();
				return responseCode == HttpURLConnection.HTTP_OK;
			} catch (UnknownHostException noInternetConnection) {
				return false;
			}
		} catch (Exception e) {
			
		  return false;
		}
	}

}
