package com.econorma.logic;

import java.util.List;

import com.econorma.data.Devices;
import com.econorma.push.android.AlarmFcmNotification;
import com.econorma.push.android.FirebaseFcmService;
import com.econorma.push.apple.ApplePushyService;
import com.econorma.util.Logger;

public class SendMessages {

	private static final Logger logger  = Logger.getLogger(SendMessages.class);
	private static final String TAG = "SendMessages";

	private final String ANDROID = "Android";
	private final String APPLE = "iOS";


	private List<Devices> devices;

	public SendMessages(List<Devices> devices) {
		this.devices=devices;
	}

	public void send() {
		 
			for (Devices device: devices) {

				String token = device.getRegistration_id();
				String message = "Software Temperature FT105RF-Plus non raggiungibile";

				if (device.getPlatform().equals(APPLE)) {
					ApplePushyService applePushy = new ApplePushyService();
					applePushy._send(token, message);
				}
				if (device.getPlatform().equals(ANDROID)) {
					AlarmFcmNotification fcmNotification = new AlarmFcmNotification();
					fcmNotification.setNotification(message, "Allarme Software");
					fcmNotification.setTo(token);
					FirebaseFcmService androidPush = new FirebaseFcmService();
					androidPush._send(fcmNotification);
				}
			}

	}

	 

}
