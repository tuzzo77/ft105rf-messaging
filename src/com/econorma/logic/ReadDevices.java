package com.econorma.logic;

import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import com.econorma.data.Devices;
import com.econorma.util.Logger;
import com.google.gson.Gson;

public class ReadDevices {
	
	private static final Logger logger = Logger.getLogger(ReadDevices.class);
	private static final String TAG = "ReadDevices";
	private static final String FILE_NAME = "devices.json";
	
	public ReadDevices() {
		
	}
	
	public List<Devices> getDeviceList() {
		
		try {
			Reader reader = new FileReader(FILE_NAME);
			 Gson gson = new Gson();
	            Devices[] list = gson.fromJson(reader, Devices[].class);
	            List<Devices> devices = Arrays.asList(list);
	            logger.info(TAG, "Lettura file devices effettuata correttamente");
	            return devices;
					
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		
		return null;
		 
	}

}
