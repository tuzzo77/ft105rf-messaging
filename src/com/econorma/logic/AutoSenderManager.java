package com.econorma.logic;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import com.econorma.data.Devices;
import com.econorma.mail.SendingBlue;
import com.econorma.util.Logger;

public class AutoSenderManager {
	private static final Logger logger  = Logger.getLogger(AutoSenderManager.class);
	private static final String TAG = "AutoSenderManager";

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private int minutesDelay;

	public AutoSenderManager(int minutesDelay) {
		this.minutesDelay = minutesDelay;
	}


	public void run(){

		try {

			Timer timer = new Timer();

			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					logger.info(TAG, "Inizio esecuzione controllo server: " + sdf.format(new Date()));
					
					PingServer ip = new PingServer();
					if (ip.isReachable()) {
						logger.info(TAG, "Fine esecuzione controllo server: " + sdf.format(new Date()));
						return;
					}
					
					ReadDevices readDevices = new ReadDevices();
					List<Devices> devices = readDevices.getDeviceList();
					if (devices!=null && devices.size()>0) {
						SendMessages message = new SendMessages(devices);
						message.send();
					}
					
					ReadMails readMails = new ReadMails();
					String[] mails = readMails.getMails();
					
					if (mails!= null && mails.length>0) {
						SendingBlue sb = new SendingBlue();
						sb.post(mails);						
					}
										
					logger.info(TAG, "Fine esecuzione controllo server: " + sdf.format(new Date()));

				}

			}, 0, TimeUnit.MILLISECONDS.convert(minutesDelay, TimeUnit.MINUTES)); 


		} catch (Exception e) {

		}

	}
}
