package com.econorma.mail;
 

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.econorma.data.Message;
import com.econorma.data.Recipient;
import com.econorma.data.Sender;
import com.econorma.util.Logger;
import com.google.gson.Gson;

public class SendingBlue {
	
	private static final String API_URL = "https://api.sendinblue.com/v3/smtp/email";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat sdfTime = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private static final Logger logger = Logger.getLogger(SendingBlue.class);
	private static final String TAG = "SendingBlue";
	private static final String mailToken = "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";
	
	public SendingBlue() {
	 
	}
	
	public void post(String[] adresses) {
		
		StringBuilder sb = new StringBuilder();
		Message message = new Message();
		message.setSender(new Sender("Econorma Sonde", "sonde@econorma.com"));
	 
		List<Recipient> recipients = new ArrayList<Recipient>();
		
		for (int i = 0; i < adresses.length; i++) {
			Recipient recipient = new Recipient(adresses[i], "Responsabile");
			recipients.add(recipient);	
		}
		
		message.setRecipient(recipients);
		
		String messagge = "Software Temperature FT105RF-Plus non raggiungibile ";
		
		try {
			message.setSubject(messagge + sdfTime.format(new Date()));

		} catch (Exception e) {
			e.printStackTrace();
		}


		try {

			sb.append("\n");
			sb.append(messagge);
		 
			message.setHtmlContent(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	 	
		Gson gson = new Gson();
		String json = gson.toJson(message);
		logger.debug(TAG, json);  
		
		try {
			 URL url = new URL(API_URL);
		 
		        URLConnection conn = url.openConnection();
		        conn.setDoOutput(true);
		        conn.setRequestProperty("content-type", "application/json");
		        conn.setRequestProperty("api-key", mailToken);
		        conn.setRequestProperty("accept", "application/json");
		 
		        try (DataOutputStream dos = new DataOutputStream(conn.getOutputStream())) {
		            dos.writeBytes(json);
		        }
		 
		        try (BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
		            String line;
		            while ((line = bf.readLine()) != null) {
		            	logger.info(TAG, line); 
		            }
		        }
				
		} catch (Exception e) {
			logger.error(TAG, e);  
		}
		
	}
 
}
