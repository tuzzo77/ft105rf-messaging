package com.econorma.data;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Message {
	
	public Sender sender;
	@SerializedName("to") 
	public List<Recipient> recipient;
	public String subject;
	public String htmlContent;
	
	public Sender getSender() {
		return sender;
	}
	public void setSender(Sender sender) {
		this.sender = sender;
	}
	public List<Recipient> getRecipient() {
		return recipient;
	}
	public void setRecipient(List<Recipient> recipient) {
		this.recipient = recipient;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getHtmlContent() {
		return htmlContent;
	}
	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}
  

}
